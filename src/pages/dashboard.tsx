import styles from '@/styles/Home.module.css'
import {TableComponent} from "@/components/table";



export default function Dashboard() {
  return (
    <>
      <main className={styles.main}>
            <TableComponent />
      </main>
    </>
  )
}
