import styles from '@/styles/Home.module.css'
import {Funds} from "@/components/SmallComponents/FundraiseComponent";



export default function Fundraise() {
    return (
        <>
            <main className={styles.main}>
                <Funds/>
            </main>
        </>
    )
}
