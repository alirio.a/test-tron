import '@/styles/globals.css'
import type { ReactElement, ReactNode } from 'react';
import type { AppProps } from 'next/app';
import Head from 'next/head';
import { AuthConsumer, AuthProvider } from '@/contexts/Web3Context';
import { NextPage } from 'next';
/*import { useEffect } from 'react';
import { UserService } from '@/services/user.service';*/
// @ts-ignore
import {toast, ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { LocaleProvider } from '@/contexts/internatiolizationContext';


type NextPageWithLayout = NextPage & {
    getLayout?: (page: ReactElement) => ReactNode;
};

interface MyAppProps extends AppProps {
    Component: NextPageWithLayout;
}

export default function App(props: MyAppProps) {
    const { Component, pageProps } = props;
    const getLayout = Component.getLayout ?? ((page) => page);

    /*  useEffect(() => {
        UserService.loginAuth();
      }, []);*/

    return (
        <>
            <Head>
                <title>Partners </title>
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
            </Head>
            <AuthProvider>

                <LocaleProvider>
                    <ToastContainer
                        position="bottom-right"
                        autoClose={8000}
                        hideProgressBar={false}
                        newestOnTop={false}
                        closeOnClick
                        rtl={false}
                        pauseOnFocusLoss
                        draggable
                        pauseOnHover
                        theme="dark"

                    />
                            <AuthConsumer>
                                {(auth) =>
                                    !auth.isInitialized ? (
                                        <>Loading.... </>
                                    ) : (
                                        getLayout(
                                            <>
                                                <Component {...pageProps} />
                                            </>)
                                    )
                                }
                            </AuthConsumer>
                </LocaleProvider>
            </AuthProvider>
        </>
    );
}
