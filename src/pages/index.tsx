import styles from '@/styles/Home.module.css'
import {Connect} from "@/components/SmallComponents/Connect";




export default function Home() {
  return (
    <>
      <main className={styles.main}>
            <Connect/>
      </main>
    </>
  )
}
