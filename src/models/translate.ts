import {User} from "@/models/user";

export interface TranslateInterface {
    signature: {
        signContract: {
            contractTitle: string,
            signSubtitle: string,
            contractSubtitle: string,
            realNameInputText: string,
            identificationInputText: string,
            uploadSignSutitle: string,
            placeholderSign: string
            initialPart: string
            idPart: string
            middlePart: string
            lastPart: (user: User) => string
    },
    kyc: {
        kycText: string,
        kycTakePhotoSubtitle: string
        kycUploadPhotoSubtitle: string
        kycPreviewSubtitle: string
        kycSubtitle: string
        kycList: string
        },
        congrats: {
            congratsTitle: string
            congratsText: (user: any) => string,
        }
    },
    stepByStep: {
        step1:{
            title: string
            text1: string
            text2: string
        },
        step2:{
            title: string
            text1: string
            text2: string
        },
        step3:{
            title: string
            text1: string
            text2: string
        },
        step4:{
            title: string
            text1: string
            text2: string
            text3: string
        }
        step5:{
            title: string
        }
    },
    dashboard: {
        salute: string
        account: string
        seedCapital: string
        seedCapitalText: string
        claimedLands: string
        unclaimedLands: string
        unclaimedLandsText: string
        aboutLandian: string
        landianLand: string
        lastLandsClaimed: string
        noTx: string
        tx: string
        lands: string,
        landianAssets: String;
        modalTitle: string
        modalCardText: string
        modalCofirm: string
        modalInput: string
        modalcheck: string
    },
    buttons: {
        clear: string
        next: string
        downloadContract: string
        woWallet: string
        wWallet: string
        seeMore: string
        continue: string
        tutorials: string
        modalButtonText: string
        modalButtonText2: string
        goBack: string
        wconnect: string
        metamask: string
        android: string
        ios: string
        translate: string
        btnModalConfirm: string
        btnModalCancel: string
    },
    index:{
        sidebar: string
        heroTitle: string
        heroText: string
        helpText: string
    },
    login:{
        title: string
        text: string
        tutorial: string
    },
    errors:{
        noUser: string,
        unverifiedUser: string,
        noIdentification: string,
        noRealName: string,
        noSignature: string,
        noKYC: string,
    },
    faqs:{
        "title": string,
        1: {
            title: string,
            text: string,
        },
        2: {
            title: string,
            text: string,
        }
        3: {
            title: string,
            text: string,
        }
        4: {
            title: string,
            text: string,
        }

        5: {
            title: string,
            text: string,
        }
        6: {
            title: string,
            text: string,
        }
        7: {
            title: string,
            text: string,
        }
    }
}
