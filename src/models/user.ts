export interface User {
  id: number;
  xifra_id: number;
  username: string;
  name: string;
  email: string;
  wallet: string;
  identification: string;
  token: string;
  amount_debt: number;
  amount_land: number;
  url_contract: string;
  sent_first_mail_at: Date;
  opened_first_mail_at: Date;
  signed_at: Date;
  url_kyc: string;
  country: null;
  sent_claim_mail_at: Date;
  opened_claim_mail_at: Date;
  sent_confirmation_mail_at: Date;
  opened_confirmation_mail_at: Date;
  verification_comments: string;
  verification: boolean;
  language: null;
  created_at: Date;
  updated_at: Date;
}
