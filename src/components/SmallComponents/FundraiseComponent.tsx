import styled from "styled-components";
import {device} from "@/utils/devices";
import {useState} from "react";
import {approveERC20, depositERC20} from "@/utils/ethers";
import {useAuth} from "@/hooks/useAuth";
import {useRouter} from "next/router";

const ConnectContainer = styled.div`
  margin: 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  border-radius: 8px;
  height: 80vh;
  width: 90vw;
  isolation: isolate;
  color: #fff;
  position: relative;
  text-align: center;

  h1 {

    z-index: 1;
    position: relative;
    font-weight: 600;

  }

  h2 {
    z-index: 1;
    position: relative;
    font-weight: 600;
    padding: 0 0 1rem 0;

    span {
      text-decoration: underline #fff 2px;

    }
  }

  @media ${device.tablet} {
    max-width: 600px;
    max-height: 440px;
  }

  small {
    color: #dad7d7;
    padding-top: 4px;
  }
`;

const InputButtonContainer = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    width: 100%;
    margin: 0;
    padding: 0;
    gap : 0;
    input{
        width: 60%;
        height: 100%;
        border: none;
        border-radius: 8px 0 0 8px;
        padding: 1rem;
        font-size: 1.2rem;
        font-weight: 600;
        color: #222;
       &:focus{
        outline: none;
       }
    }
    button{
        width: 40%;
        height: 100%;
        border: none;
        border-radius: 0 8px 8px 0;
        padding: 1rem;
        font-size: 1.2rem;
        font-weight: 600;
        cursor: pointer;
        &:hover{
         background: #dad7d7;
            color: #222;
        }
    }
`;


export const Funds = () => {
    const [value, setValue] = useState('');
    const router = useRouter();
    const { provider } = useAuth();
    const formatCurrency = (number) => {
        return new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 0,
            maximumFractionDigits: 0
        }).format(number);
    };

    const parseInputValue = (inputValue) => {
        return inputValue.replace(/[^\d.]/g, '');
    };

    const handleChange = (event) => {
        const inputValue = event.target.value;
        const parsedValue = parseInputValue(inputValue);

        // Validar si el valor ingresado es numérico con o sin signo de dólar
        if (parsedValue === '' || /^\d+(\.\d{0,2})?$/.test(parsedValue)) {
            const formattedValue = parsedValue ? formatCurrency(parseFloat(parsedValue)) : '';
            setValue(formattedValue);
        }
    };

    const fundRaiseTheProject = async() => {
        console.log('funding the project');
        try{
            const approve = await approveERC20(parseInputValue(value), provider);
            console.log(approve);

            const fundraise = await depositERC20(parseInputValue(value), provider);
            console.log(fundraise);

            router.push('/dashboard');

        } catch (e) {
            console.log(e);
        }
    };

    return <>
        <ConnectContainer>
            <div>
                <h1>¡Estamos listos!</h1>
                <h2>Ahora debes fondear el contrato: </h2>
            </div>

            <InputButtonContainer>
                <input type="text" placeholder="$1,000,000" value={value} onChange={handleChange} />
                <button onClick={() => fundRaiseTheProject()}>Fondear</button>
            </InputButtonContainer>
            <small>Ingrese la cantidad a fondear en USDT</small>

        </ConnectContainer>
    </>

}