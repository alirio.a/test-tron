import styled from "styled-components";
import {device} from "@/utils/devices";
import Image from "next/image";
import {useAuth} from "@/hooks/useAuth";
import {useEffect} from "react";
import {useRouter} from "next/router";

const ConnectContainer = styled.div`
  margin: 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  border-radius: 8px;
  height: 80vh;
  width: 90vw;
  isolation: isolate;
  color: #fff;
  position: relative;
  text-align: center;
  div{
    padding-bottom: 2rem;
  }
  h1{

    z-index: 1;
    position: relative;
    font-weight: 600;
    padding: 1rem 2rem;

  }
    h2{
      z-index: 1;
      position: relative;
        font-weight: 600;
        padding: 0 2rem;
      span{
        text-decoration: underline #fff 2px;

      }
    }
  @media ${device.tablet} {
    max-width: 600px;
    max-height: 440px;
  }
`;

const ConnectButton1 = styled.button`
    position: relative;
    color: #FFF;
    background: transparent;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    border: none;
    border-radius: 8px;
    font-size: 1.2rem;
    font-weight: 600;
    width: 85%;
    cursor: pointer;
    transition: all 0.2s ease-in-out;
    &:before {
      content: '';
      position: absolute;
      bottom: 0;
      left: 0;
      width: 100%;
      height: 100%;
      z-index: 1;
      opacity: 0;
      transition: all 0.3s;
      border-top-width: 1px;
      border-bottom-width: 1px;
      border-top-style: solid;
      border-bottom-style: solid;
      border-top-color: rgba(255,255,255,0.5);
      border-bottom-color: rgba(255,255,255,0.5);
      transform: scale(0.1, 1);
    }
    &:hover {
       letter-spacing: 2px;
    }
    &:hover::before {
      opacity: 1;
      transform: scale(1, 1);
    }
    &:after {
      content: '';
      position: absolute;
      bottom: 0;
      left: 0;
      width: 100%;
      height: 100%;
      z-index: 1;
      transition: all 0.3s;
      background-color: rgba(255,255,255,0.1);
    }
    &:hover::after {
      opacity: 0;
      transform: scale(0.1, 1);
    }
`;



export const Connect = () => {

    const {loginWalletConnect, account} = useAuth()
    const router = useRouter();

    useEffect(() => {
        if(account){
            console.log("account", account)
            router.push("/fundraise");
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [account])
    return <>
            <ConnectContainer>
                <div>
                    <h1>¡Bienvenido!</h1>
                    <h2>Gracias por confiar en nosotros y en el proyecto  <span>XXXXX</span> </h2>
                </div>
                <ConnectButton1 onClick={() => loginWalletConnect()}>Connect with WalletConnect <Image src={"/assets/walletConnect.svg"} width={100} height={100} alt={"wc"}  /></ConnectButton1>

            </ConnectContainer>
    </>

}