import styled from "styled-components";
import {useEffect, useState} from "react";
import {getCurrentPaymentDelivered, getSignersAndStatus, sign} from "@/utils/ethers";
import {useAuth} from "@/hooks/useAuth";

const TableContainer = styled.div`
  
  max-width: 90vw;
  overflow-y: auto;
  max-height: 80vh;
  table {
    border: 1px solid #fff;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    width: 100%;
    table-layout: fixed;
    color: white;
  }

  table caption {
    font-size: 1.5em;
    margin: .5em 0 .75em;
  }

  table tr {
    background-color: transparent;
    border: 1px solid #ddd;
    padding: .35em;
  }

  table th,
  table td {
    padding: .625em;
    text-align: center;
  }

  table th {
    font-size: .85em;
    letter-spacing: .1em;
    text-transform: uppercase;
  }

  @media screen and (max-width: 600px) {
    table {
      border: 0;
    }

    table caption {
      font-size: 1.3em;
    }

    table thead {
      border: none;
      clip: rect(0 0 0 0);
      height: 1px;
      margin: -1px;
      overflow: hidden;
      padding: 0;
      position: absolute;
      width: 1px;
    }

    table tr {
      border-bottom: 3px solid #ddd;
      display: block;
      margin-bottom: .625em;
    }

    table td {
      border-bottom: 1px solid #ddd;
      display: block;
      font-size: .8em;
      text-align: right;
    }

    table td::before {
      /*
      * aria-label has no advantage, it won't be read inside a table
      content: attr(aria-label);
      */
      content: attr(data-label);
      float: left;
      font-weight: bold;
      text-transform: uppercase;
    }

    table td:last-child {
      border-bottom: 0;
    }
  }

`;

const ApproveButton = styled.button<{approved: boolean}>`
  padding: 0.6em 2em;
  outline: none;
  color: ${props => props.approved ? "rgb(0, 0, 0)" : "rgb(255, 255, 255)"};
  background: ${props => props.approved ? "rgb(255,255,255)": "transparent" } ;
  border: 2px solid rgb(255, 255, 255);
  font-weight: 600;
  cursor: pointer;
  position: relative;
  z-index: 0;
  border-radius: 10px;
  user-select: none;
  -webkit-user-select: none;
  touch-action: manipulation;
  transition: 0.2s all ease-in-out;
  &:hover {
    background: rgb(255, 255, 255);
    color: rgb(0, 0, 0);
    transition: 0.2s all ease-in-out;
  }
  &:disabled {
    background: #ddd;
    color: #666;
  }
  
`;

export const TableComponent = () =>{

    const [currentPayment, setCurrentPayment] = useState<any>(0);
    // @ts-ignore
    const [totalAmount, setTotalAmount] = useState<any>(0);
    // @ts-ignore
    const [totalPayments, setTotalPayments] = useState<any>(0);
    const [financialSigned, setFinancialSigned] = useState<any>(false);
    // @ts-ignore
    const [accountantSigned, setAccountantSigned] = useState<any>(false);
    const [financialApproval, setFinancialApproval] = useState<any>("");
    const [accountantApproval, setAccountantApproval] = useState<any>("");

    const {account, provider} = useAuth();

    const handleApprove = async () => {
        console.log("Approve");
        await sign(provider);
    }

    useEffect(() => {
        getSignersAndStatus().then((res: any) => {
            console.log(res);
            setFinancialApproval(res[0]);
            setFinancialSigned(res[1]);
            setAccountantApproval(res[2]);
            setAccountantSigned(res[3]);
        });
        getCurrentPaymentDelivered().then((res: any) => {
            setCurrentPayment(Number(res));
        });
    }, []);

    return (
        <TableContainer>
            <table>
                <caption>Desembolsos</caption>
                <thead>
                <tr>
                    <th scope="col">Desembolso</th>
                    <th scope="col">Fecha de resuelto</th>
                    <th scope="col">Monto</th>
                    <th scope="col">Aprobacion F</th>
                    <th scope="col">Aprobacion A</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td data-label="Desembolso">1</td>
                    <td data-label="Fecha de resuelto">04/04/2023</td>
                    <td data-label="Monto">$1,000.000</td>
                    <td data-label="Aprobacion Financiador"> <ApproveButton
                        approved={currentPayment > 0 || financialSigned}
                        disabled={account !== financialApproval}
                        onClick={() => handleApprove()}
                        >
                        {currentPayment > 0 || financialSigned ? "Aprobado" : "Aprobar"}
                    </ApproveButton></td>
                    <td data-label="Aprobacion Administrador"> <ApproveButton
                        approved
                        disabled={account !== accountantApproval}
                        onClick={() => handleApprove()}
                    >
                        {currentPayment > 0 ? "Aprobado" : "Aprobar"}
                    </ApproveButton></td>
                </tr>
                <tr>
                    <td scope="row" data-label="Desembolso">2</td>
                    <td data-label="Fecha de resuelto">04/05/2023</td>
                    <td data-label="Monto">$1,000.000</td>
                    <td data-label="Aprobacion Financiador"> <ApproveButton
                        approved={currentPayment > 1}
                        disabled={account !== financialApproval}
                        onClick={() => handleApprove()}
                    >
                        {currentPayment > 0 ? "Aprobado" : "Aprobar"}
                    </ApproveButton></td>
                    <td data-label="Aprobacion Administrador"> <ApproveButton
                        approved={currentPayment > 2}
                        disabled={account !== accountantApproval}
                        onClick={() => handleApprove()}
                    >
                        {currentPayment > 0 ? "Aprobado" : "Aprobar"}
                    </ApproveButton></td>
                </tr>
                <tr>
                    <td scope="row" data-label="Desembolso">3</td>
                    <td data-label="Fecha de resuelto">04/06/2023</td>
                    <td data-label="Monto">$1,000,000</td>
                    <td data-label="Aprobacion Financiador"> <ApproveButton
                        approved={currentPayment > 3}
                        disabled={account !== financialApproval}
                        onClick={() => handleApprove()}
                    >
                        {currentPayment > 0 ? "Aprobado" : "Aprobar"}
                    </ApproveButton></td>
                    <td data-label="Aprobacion Administrador"> <ApproveButton
                        approved
                        disabled={account !== accountantApproval}
                        onClick={() => handleApprove()}
                    >
                        {currentPayment > 0 ? "Aprobado" : "Aprobar"}
                    </ApproveButton></td>
                </tr>
                <tr>
                    <td scope="row" data-label="Desembolso">4</td>
                    <td data-label="Fecha de resuelto">04/07/2023</td>
                    <td data-label="Monto">$1,000,000</td>
                    <td data-label="Aprobacion Financiador"> <ApproveButton
                        approved={currentPayment > 4}
                        disabled={account !== financialApproval}
                        onClick={() => handleApprove()}
                    >
                        {currentPayment > 0 ? "Aprobado" : "Aprobar"}
                    </ApproveButton></td>
                    <td data-label="Aprobacion Administrador"> <ApproveButton
                        approved
                        disabled={account !== accountantApproval}
                        onClick={() => handleApprove()}
                    >
                        {currentPayment > 0 ? "Aprobado" : "Aprobar"}
                    </ApproveButton></td>
                </tr>
                </tbody>
            </table>
        </TableContainer>
    )
}