
const url: string = process.env.NEXT_PUBLIC_BASE_API_URL || '';
const version: string = process.env.NEXT_PUBLIC_BASE_API_VERSION || '';

const user: string = process.env.NEXT_PUBLIC_SERVICE_USER || '';
const key: string = process.env.NEXT_PUBLIC_SERVICE_PASSWORD || '';

export interface User {
    username: string;
    email: string;
    wallet: string;
    reference_client_id: number;
    id: number;
    role_id: number;
    status_id: number;

}

export class UserService {
    static async setWalletByClient(wallet: string, token: string): Promise<{ status: string, message: string, data?: User }> {
        const tokenAuth = localStorage.getItem("auth");
        const response = await fetch(`${url}/${version}/clients/set-wallet-by-token`, {
            method: "POST",
            body: JSON.stringify({ wallet, token }),
            headers: {
                'authorization': `bearer ${tokenAuth}`,
                'content-type': 'application/json',
            },
        })
        // console.log(`${url}/${version}/user/valid/wallet/${wallet}`);
        const data = await response.json();
        // console.log(data);
        return data;
    }

    static async setContractByToken(payload: any): Promise<any> {
        const tokenAuth = localStorage.getItem("auth");
        const response = await fetch(`${url}/${version}/clients/set-contract-by-client`, {
            method: "POST",
            body: JSON.stringify(payload),
            headers: {
                'authorization': `bearer ${tokenAuth}`,
                'content-type': 'application/json',
            },
        })
        // console.log(`${url}/${version}/user/valid/wallet/${wallet}`);
        const data = await response.json();
        // console.log(data);
        return data;
    }

    static async setKYC(payload: any): Promise<any> {
        const tokenAuth = localStorage.getItem("auth");
        const response = await fetch(`${url}/${version}/clients/set-kyc-by-client`, {
            method: "POST",
            body: JSON.stringify(payload),
            headers: {
                'authorization': `bearer ${tokenAuth}`,
                'content-type': 'application/json',
            },
        })
        // console.log(`${url}/${version}/user/valid/wallet/${wallet}`);
        const data = await response.json();
        // console.log(data);
        return data;
    }
    static async validateUser(user: string) {
        const token = localStorage.getItem("auth");
        const response = await fetch(`${url}/${version}/clients/user-by-wallet?wallet=${user}`, {
            method: "GET",
            headers: {
                'authorization': `bearer ${token}`,
            },
        })
        // console.log(`${url}/${version}/user/valid/wallet/${wallet}`);
        const data = await response.json();
        // console.log(data);
        return data;
    }

    static async validateUserByToken(tokenUser: string) {
        const token = localStorage.getItem("auth");
        const response = await fetch(`${url}/${version}/clients/user-by-token?token=${tokenUser}`, {
            method: "GET",
            headers: {
                'authorization': `bearer ${token}`,
            },
        })

        console.log(`${url}/${version}/clients/user-by-token?token=${tokenUser}`);
        const data = await response.json();
        console.log(data);
        return data;
    }


    static async loginAuth() {
        const response = await fetch(`${url}/${version}/auth/login`, {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                user,
                key,
            })
        });

        const data = await response.json();
        //console.log(data)
        localStorage.setItem("auth", data.data.token);
        return data;
    }

}