import { useContext } from 'react';
import { Web3Context } from 'src/contexts/Web3Context';

export const useAuth = () => useContext(Web3Context);
