export const extractNumber = (currencyString) => {
    const currencyRegex = /^\$?(\d{1,3}(\d{3})*|(\d+))(\.\d{2})?$/;

    if (currencyRegex.test(currencyString)) {
        const onlyNumbers = currencyString.replace(/[^\d.]/g, '');
        return parseFloat(onlyNumbers);
    } else {
        throw new Error('Invalid currency format');
    }
};