import {ethers, providers} from "ethers";
import ERC20_ABI from "./abi-erc20.json";
import DUALSIGN_ABI from "./abi-dualSign.json";

const USDT_ADDRESS = process.env.NEXT_PUBLIC_USDT_ADDRESS as string;
const RPC = process.env.NEXT_PUBLIC_RPC_URL as string;
const FUNDING_CONTRACT = process.env.NEXT_PUBLIC_ADDRESS_FUNDS as string;

const RPC_PROVIDER = new ethers.providers.JsonRpcProvider(RPC);


const USDT_RPC = new ethers.Contract(
    USDT_ADDRESS,
    ERC20_ABI,
    RPC_PROVIDER
)

export const approveERC20 = async (_amount: number, providerWEB3: any) => {
    // @ts-ignore
    const provider = new providers.Web3Provider(providerWEB3);
    const signer = provider.getSigner();
    const usdtContract = new ethers.Contract(USDT_ADDRESS, ERC20_ABI, signer);
    try {
        const tx = usdtContract.connect(signer);
        const data = await tx.approve(
            FUNDING_CONTRACT,
            ethers.utils.parseUnits(_amount.toString())
        );
        await data.wait();
        return data.hash;
    } catch (e) {
        throw new Error("Error approving the spender the right amount");
    }
};

export const getUserTokens = async (wallet: string) => {
    const usdt = await USDT_RPC.balanceOf(wallet);

    return {
        usdt: Number(ethers.utils.formatEther(usdt)),
    }
};

export const depositERC20 = async ( _amount: number, providerWEB3: any) => {
    const provider = new providers.Web3Provider(providerWEB3);
    const signer = provider.getSigner();
    const depositContract = new ethers.Contract(FUNDING_CONTRACT, DUALSIGN_ABI, signer);
    try {
        const tx = depositContract.connect(signer);
        const data = await tx.depositUSDT(_amount);
        await data.wait();
        return data.hash;
    } catch (e) {
        throw new Error("Error depositing the tokens");
    }
};

export const sign = async (providerWEB3: any) => {
    const provider = new providers.Web3Provider(providerWEB3);
    const signer = provider.getSigner();
    const depositContract = new ethers.Contract(FUNDING_CONTRACT, DUALSIGN_ABI, signer);
    try {
        const tx = depositContract.connect(signer);
        const data = await tx.sign();
        await data.wait();
        return data.hash;
    } catch (e) {
        throw new Error("Error signing the tokens");
    }
};

export const getSignersAndStatus = async () => {
    const depositContract = new ethers.Contract(FUNDING_CONTRACT, DUALSIGN_ABI, RPC_PROVIDER);
    try {
        return await depositContract.getSignersAndStatus();
    } catch (e) {
        throw new Error("Error getting signers and status");
    }

}

export const getCurrentPaymentDelivered = async () => {
    const depositContract = new ethers.Contract(FUNDING_CONTRACT, DUALSIGN_ABI, RPC_PROVIDER);
    try {
        return await depositContract.getCurrentPaymentDelivered();
    } catch (e) {
        throw new Error("Error getting current payment delivered");
    }

}