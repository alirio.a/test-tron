import WalletConnectProvider from "@walletconnect/web3-provider";
import { IProviderOptions } from "web3modal";

export const providerOptions: IProviderOptions = {
    /* See Provider Options Section */
    walletconnect: {
        package: WalletConnectProvider,
        options: {
            rpc: {
                //56: "https://bsc-dataseed2.binance.org",
                97: "https://data-seed-prebsc-1-s1.binance.org:8545",
            }
            ,
            //chainId: 56,
            chainId: 97,
        },
    },
}