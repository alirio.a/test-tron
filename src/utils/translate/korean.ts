import { TranslateInterface } from '@/models/translate';
import {User} from "@/models/user";

export const koreanTranslate: TranslateInterface = {
    "signature": {
        "signContract": {
            "contractTitle": "Xifra' 자본에 대한 Metaverse Lot 자발적 이전",
            "contractSubtitle": "",
            "signSubtitle":"디지털 서명을 수행하십시오.",
            "uploadSignSutitle":"기호 업로드",
            "realNameInputText": "채권자 이름",
            "identificationInputText": "ID 번호",
            "placeholderSign": "여기에 서명",
            "initialPart": "나",
            "idPart": "그리고 lo의 식별",
            "middlePart":"",
            "lastPart": (user: User)=> (`
            이 계약(Xifra의 소유권/지갑의 전체 식별) 가입을 통해 Landian의 메타버스 부지로 교환하는 것이 나의 합법적이고 자유롭고 방해받지 않는 의도임을 명시하고 이에 따라 Landian 메타버스 내에서 ${user.amount_land}를 수락합니다. 유니버스는 ${user.amount_debt}의 금액으로 내 서명이 첨부된 날 내 지갑에 따라 내가 xfira에 대해 가질 수 있는 모든 모든 권리에 대해 전액 지불합니다. 이에 따라 Landian 메타버스 내에서 200 USD의 환율로 ${user.amount_land} 로트를 받고 이로써 내가 Xifra에 대해 가질 수 있는 모든 권리 또는 조치를 양도합니다.
            <br/> <br/>
또한, 나는 Landian이 나 또는 제3자에 대해 Xifra에 의해 발생한 모든 부채에 대해 책임이 없으며 앞으로도 책임이 있으며 그 책임이 Landian Metaverse 부지의 실행 가능성, 가치 및 존재 내에 있음을 완전히 인정합니다.
 <br/><br/>
각 부분은 1구획이 300달러 이상에 해당하며 이러한 금액은 아래에 설정된 거래 비율이라는 데 동의합니다.
<br/><br/>
이 계약이 완료되면 모든 양도가 완료되고 각 자산에 대한 두 소유권이 확정된 후에만 양 당사자가 선의의 수신 문서에 서명해야 합니다.
            `),
        },
    "kyc": {
        "kycText": `이제 신분증 옆에 있는 얼굴 사진(정면 보기)이 필요합니다. 이를 위해 사진을 찍거나 이미지 라이브러리에서 업로드할 수 있습니다. 다음으로 업로드할 사진의 예를 보여줍니다.`,
        "kycSubtitle": "무엇을 고려해야 합니까?",
        "kycTakePhotoSubtitle": "사진을 찍다",
        "kycUploadPhotoSubtitle": "사진 업로드",
        "kycPreviewSubtitle": "참조 이미지",
        "kycList": `
        <ol type='1'>
        <li>
            사진의 초점이 맞는지 확인합니다.
        </li>
        <li>
            문서는 완전히 보여야 합니다.
        </li>
        <li>
            팔과 몸통이 보여야 합니다.
        </li>
        <li>
            머리가 잘리지 않아야 합니다.
        </li>
    </ol>`
    },
    "congrats": {
        "congratsTitle": "준비 완료!",
        "congratsText": (user) => (`
            가상 서명을 만들어 주셔서 감사합니다. 유효성 검사 프로세스는 1~15일이 소요될 수 있습니다. 프로세스를 조금 더 빠르게 진행하려면 계약서를 다운로드하고 실제 서명과 함께 이메일 agreement@myxifralifestyle.com으로 보내주십시오.
            이것이 확인되면 ${user.email} 으로 로트 청구 지침이 포함된 이메일이 전송됩니다.
        `)
    }
},
    "stepByStep": {
        "step1":{
            "title": "1 단계",
            "text1": `BEP20 지갑이 있으면 좋을 것입니다. 트러스트 월렛을 추천합니다.`,
            "text2": `
            <ol type={"A"} style={{ "marginTop": "50px", "fontSize": "20px", "fontWeight": "700", "padding": "22px" }}>
                <li>
                    운영 체제의 앱 스토어에서 Trust Wallet을 검색하십시오.
                </li>
                <li>
                    설치 / 가져오기를 누릅니다.
                </li>
                <li>
                    앱을 엽니다.
                </li>
            </ol>
            `
        },
        "step2":{
            "title": "2 단계",
            "text1": `새 계정을 만들려면 다음 단계를 따르십시오.`,
            "text2": `
            <ol type={"A"}>
                <li>
                새 지갑 만들기를 누릅니다.
                </li>
                <li>
                이용 약관에 동의한 다음 계속하십시오.
                </li>
            </ol>
            `
        },
        "step3":{
            "title": "3 단계",
            "text1": `비밀 문구를 복사하여 안전한 장소에 보관하십시오. 누구와도 공유하지 마십시오. 비밀 문구를 사용하면 귀하 또는 다른 사람이 모든 모바일 장치에서 지갑을 열고 디지털 자산에 완전히 액세스할 수 있습니다. 비밀 문구를 분실하면 계정을 복구할 수 없습니다.`,
            "text2": `잃어버리지 말고 공유하지 마세요`
        },
        "step4":{
            "title": "4 단계 ",
            "text1": `비밀 문구를 확인하기 위해 올바른 순서로 단어를 입력합니다.`,
            "text2": `잘했어요! 성공적으로 계정을 만들었으며 이제 토지를 청구할 수 있습니다.`,
            "text3": `로그인하자`
        },
        "step5":{
            "title": "5 단계",
        }
    },
    "dashboard": {
        "salute": "안녕하세요",
        "account": "지갑:",
        "seedCapital": "나의 시드 캐피탈",
        "seedCapitalText": "이 값은 이전 구매에 해당합니다.",
        "claimedLands": "소유권이 주장된 부지",
        "unclaimedLands": "청구 프로세스의 배치",
        "unclaimedLandsText":"이 프로세스는 1~15일이 소요될 수 있습니다. 지갑에서 로트를 사용할 수 있게 되면 이메일을 받게 됩니다.",
        "aboutLandian": `<span>자세히 알아보기</span>
        <br /> LANDIAN 메타버스`,
        "landianLand":"Landian 국가",
        "lastLandsClaimed":"마지막으로 소유권을 주장한 땅",
        "noTx": "거래 없음",
        "tx": "거래:",
        "lands": "많이",
        "landianAssets": "랜디안 자산 계약",
        "modalTitle": "NFT 전송",
        "modalCardText": "Binance Smart Chain 네트워크에서 배송 주소가 지원되는지 확인하십시오. 배송 주소가 이 네트워크를 지원하지 않으면 자산을 잃게 됩니다.",
        "modalCofirm":"이 지갑으로의 전송을 확인하십시오.",
        "modalInput":"지갑 BEP20",
        "modalcheck": "지갑 확인"
    },
    "buttons": {
        "clear": "닦음",
        "next": "다음",
        "downloadContract": "계약서 다운로드",
        "woWallet": "지갑이 없어요",
        "wWallet": "나는 이미 지갑을 가지고 있다.",
        "continue": "계속하다",
        "seeMore":"더보기",
        "tutorials": "튜토리얼 보기",
        "modalButtonText": "땅을 보내다",
        "modalButtonText2": "보내다",
        "goBack": "반품",
        "wconnect": "WalletConnect와 연결",
        "translate": "언어",
        "metamask": "메타마스크와 연결",
        "btnModalConfirm": "확인하다",
        "btnModalCancel": "취소",
        "android": `
            <strong>Android</strong>
            <p>
                Tutorial
            </p>
      `,
        "ios": `
        <strong>IOS</strong>
        <p>
            tutorial
        </p>
    `
    },
    "index":{
        "sidebar": `
        우리는 Metaverse 를 세계에서 가장 큰 구매 커뮤니티로 가져오는 얼라이언스입니다.`,
        "heroTitle": "메타버스에 오신 것을 환영합니다!",
        "heroText": `LANDIAN 에 위치한 토지를 주장할 수 있습니다. 땅을 받을 지갑을 준비하십시오. 지갑이 없어도 걱정하지 마세요. 지갑이 없습니다로 이동하여 단계에 따라 지갑을 만드세요.`,
        "helpText": `지갑이 없어도 걱정하지 마세요. 지갑 생성 및 설치를 단계별로 도와드립니다.
        새로운 암호 지갑.`
    },
    "login":{
        "title": "지갑 연결",
        "text": `우리는 세계 최대의 구매 커뮤니티에 메타버스를 가져올 얼라이언스입니다.`,
        "tutorial": "어떻게 로그인합니까?"
    },
    "errors":{
        "noUser":"",
        "unverifiedUser":"",
        "noIdentification":"",
        "noRealName":"",
        "noSignature":"",
        "noKYC":"",
    },
    "faqs":{
        "title": "자주 묻는 질문",
        "1": {
            "title":"내 땅을 받는 데 얼마나 걸립니까?",
            "text":"플랫폼에 로그인하면 대기자 명단에 자동으로 등록됩니다. 대기 시간은 Binance Smart Chain 네트워크의 점유에 따라 달라집니다. 즉시 발송되거나 약 15일이 소요될 수 있습니다. 이 시간은 네트워크와 관련이 있으며 Landian 또는 Xifra에 의존하지 않습니다."
        },
        "2": {
            "title":"내 땅을 팔 수 있습니까?",
            "text":`
                물론. 두 가지 옵션이 있습니다.
                <ol type="1"> 
                    <li>
                        Opensea 또는 Element와 같은 마켓플레이스를 통해 글로벌 시장에 진출할 수 있습니다.
                    </li>
                    <li>
                        친구, 가족, 가까운 사람들에게 Landian 메타버스에 대해 알리고 직접 판매할 수 있습니다. 이를 P2P(Per to Per) 트랜잭션이라고 합니다. "랜드 보내기"라고 표시된 대시보드에서 랜드를 보내거나 지갑에서 직접 구매자의 지갑으로 랜드를 보낼 수 있습니다.
                    </li>
                </ol>
            `
        },
        "3": {
            "title":"Landian 땅의 가치는 얼마입니까?",
            "text": `토지의 가치는 시장에 달려 있습니다. 오늘(2023년 2월) 기준으로 사람들은 P2P(개인간) 시장에서 토지당 $200~$300에 거래합니다.
이들의 가치는 Landian의 개발 로드맵 준수, 커뮤니티 채택 및 달성한 글로벌 범위에 따라 결정됩니다.`
        },
        "4": {
            "title":"내 땅을 매물로 내놓아야 합니까?",
            "text":`이것은 개인적인 결정이지만 고려해야 할 몇 가지 사실이 있습니다.
            2022년 4월 Landian의 10x10(100m2) 토지는 20 USDT의 가치가 있었습니다. 1년이 채 지나지 않아 Landian이 프로젝트를 채택하고 지원하는 더 큰 커뮤니티에서 자신을 포지셔닝한 덕분에 토지의 가치는 최소 10배 증가했습니다.`
        },
        "5": {
            "title":"내 땅은 어디에서 볼 수 있습니까?",
            "text":`네트워크 정체로 인해 메타데이터를 로드하는 데 시간이 걸리는 경우가 있지만 걱정하지 마세요! 당신이 그것들을 볼 수 없다는 사실이 당신의 지갑에 그것들이 없다는 것을 의미하지는 않습니다.
                지갑에서 토지가 보이지 않으면 다음 단계를 따르십시오. BNB 스마트 체인 주소를 복사하고 브라우저에서 BSC SCAN을 검색한 다음 거기에 지갑 주소를 붙여넣으십시오. ERC-721 섹션에서 현재 지갑에 저장되어 있는 모든 NFT를 볼 수 있습니다. 거기에 표시된 정보는 썩지 않으므로 땅을 소유하고 있다고 믿을 수 있습니다.
                <br/>
                <br/>
                이제 Metamask 지갑이 있으면 NFT 섹션에 랜드가 자동으로 나타나지 않습니다. 다음 자습서에 따라 수동으로 추가하십시오.
                <br/><br/>
                <strong>기억:</strong>
                <ul>
                    <li>우리는 Binance Smart Chain 네트워크에서 작업합니다. 따라서 지갑이 이 네트워크에 있는지 확인해야 합니다.</li>
                    <li>Trust Wallet을 사용하는 것이 좋습니다. 그러나 Metamask와 같은 다른 지갑을 사용할 수 있습니다.</li>
                </ul>`
        },
        "6": {
            "title":"지원을 요청하는 방법?",
            "text":`
                추가 질문이 있는 경우 공식 텔레그램 채널(<a href="https://t.me/helplandianxifra" target="_blank" rel="noopener">https://t.me/helplandianxifra </a>)을 통해 지원팀에 문의하세요. 
            `
        },
        "7": {
            "title":"¿Cómo puedo enviar mis lotes en el mercado P2P (persona a persona)?",
            "text":`
                여러 가지 방법으로 할 수 있습니다. 그 중 하나는 지갑에서 가져오는 것입니다. 지갑의 NFT 섹션에 들어가 받은 NFT를 열고 "보내기" 옵션을 선택하면 가고자 하는 NFT의 대상 지갑을 복사해야 합니다. 보내려면 대상 지갑도 BEP20 네트워크에 있어야 하며 네트워크에서 트랜잭션 사용에 대한 요금을 청구할 수 있도록 지갑에 BNB 스마트 체인이 있어야 합니다.
또 다른 방법은 청구 대시보드에서 하는 것입니다. <a href="https://claim.xifraglobal.com/" target="_blank" rel="noopener">https://claim.xifraglobal.com/</ a>, 프로세스를 완료하고 지갑에 NFT가 있으면 "send land"를 선택하고 BEP20 네트워크의 대상 지갑을 붙여넣고 보내기를 선택합니다. 지불을 위해 지갑에 BNB 스마트 체인이 있어야 합니다. 네트워크가 거래에 사용하기 위해 수행하는 것이므로 지갑에서 이 작업을 수행할 때 지갑에서 거래를 승인해야 프로세스가 완료됩니다. 
            `
        },
    }
}