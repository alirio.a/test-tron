import { TranslateInterface } from "@/models/translate";
import {User} from "@/models/user";

export const englishTranslate: TranslateInterface = {
    "signature": {
        "signContract": {
            "contractTitle": "Voluntary transfer Metaverse Lots for Xifra’ capital",
            "contractSubtitle": "",
            "signSubtitle":"Please perform your digital signature",
            "uploadSignSutitle":"Upload sign",
            "realNameInputText": "Creditor fullname",
            "identificationInputText": "Enter Id Number",
            "placeholderSign": "Sign here",
            "initialPart": "I",
            "idPart": " and identificaction of lo",
            "middlePart":"",
            "lastPart": (user: User)=> (`
           it is herein stated that is my lawful, free and unhindered intention to exchange through the subscription of this agreement (full identification of title/wallet from Xifra) into Landian’s metaverse lots , and hence hereby accept ${user.amount_land} within the landian metaverse universe in full payment against any and al all right I might have against xfira in accordance with my wallet on the day my signature is hereby affixed in an amount of ${user.amount_debt} . Accordingly inhereby receive ${user.amount_land} lots within the landian metaverse at at an exchange rate of 200 USD hereby ceding all rights or actions I may have against Xifra. 
            <br/> <br/>
Furthermore, I fully recognize Landian is not and will be responsible for any and all debts incurred by Xifra against me or third parties and its responsability is framed within the viability,value and existence of the lots of the Landian Metaverse. 
 <br/><br/>
The parts agree that 1 plot shall be equivalent to 200 us Dollars and such wolk be rate of the transaction set fort below. 
<br/><br/>
Once this agreement has been finalized all transfer shall be perfected and only once both ownership  rights to each assets have been finalized both parties shall sign a duly good faith reception document.
            `),
        },
    "kyc": {
        "kycText": `Once you finished the prior step, we will need a photo of you showing your face while holding your ID card (straight-on), 
            for this you can wether take the photo or upload it from your file system. In the following image, 
            you will find an example of how the photo should look like.`,
        "kycSubtitle": "WHAT YOU NEED TO KEEP IN MIND?",
        "kycTakePhotoSubtitle": "Take photo",
        "kycUploadPhotoSubtitle": "Upload photo",
        "kycPreviewSubtitle": "Reference image",
        "kycList": `
            <ol type='1'>
            <li>
                Make sure the camera is focused properly.
            </li>
            <li>
                the id card must be fully visible.
            </li>
            <li>
                Your arms and chest must be in the photo.
            </li>
            <li>
                Your head cannot be cropped.
            </li>
        </ol>`
        },
    "congrats": {
        "congratsTitle": "All set!",
        "congratsText": (user) => (`
        Thank you for virtually signing up, the validation process migth take a few days (1 to 15 days).
        Once we make sure everything is in order, we will send you a email with the guidelines to claim your Lands to: ${user.email}.
        `)
    }
},
    "stepByStep": {
        "step1":{
            "title": "Step 1",
            "text1": `It would be good if you have a BEP20 wallet. We recommend you Trust Wallet.`,
            "text2": `

            <ol type={"A"} style={{ "marginTop": "50px", "fontSize": "20px", "fontWeight": "700", "padding": "22px" }}>
                <li>
                       In the app store of your operating system search Trust Wallet.
                </li>
                <li>
                        Tap Installing / Get. 
                </li>
                <li>
                        Open the app.
                </li>
            </ol>
            `
        },
        "step2":{
            "title": "Step 2",
            "text1": `Follow these steps to create a new account.`,
            "text2": `
            <ol type={"A"} style={{ "marginTop": "50px", fontSize: "20px", fontWeight: "700", padding: "22px" }}>
                <li>
                        Tap Create a new wallet. 
                </li>
                <li>
                        Accept Terms and Conditions and then Continue.
                </li>
            </ol>
            `
        },
        "step3":{
            "title": "Step 3",
            "text1": `Copy your Secret Phrase and keep it in a safe place. Please don't share it with anyone. With your Secret Phrase, you or someone else can open your wallet on any mobile device and fully access your digital assets. You can't recover your account if you lose your Secret phrase.`,
            "text2": `Don't lose it, don't share it!`
        },
        "step4":{
            "title": "Step 4",
            "text1": `Put the words in the correct order to verify your secret phrase. `,
            "text2": `<span>Well Done!</span>  You have successfully created your account, and now you can claim your lands.`,
            "text3": `Let’s Log in`
        },
        "step5":{
            "title": "Step 5",
        }
    },
    "dashboard": {
        "salute": "Hello",
        "account": "Wallet:",
        "seedCapital": "My Seed capital",
        "seedCapitalText": "This value corresponds to your lots claimed.",
        "claimedLands": "Claimed lands",
        "unclaimedLands": "Lands in claim process",
        "unclaimedLandsText":"This process might take around of 1-15 days. We will email you once your lands are on your wallet.",
        "aboutLandian": `<span>Learn more about</span>
        <br /> LANDIAN METAVERSE`,
        "landianLand":"Landian Land",
        "lastLandsClaimed":"Last Lands Claimed",
        "tx": "Transaction",
        "noTx": "NO TRANSACTION",
        "lands": "Lands",
        "landianAssets": "Landian assets contract",
        "modalTitle": "Transfer NFTs",
        "modalCardText": "Make sure the shipping address is supported on the Binance Smart Chain network. If the shipping address does not support this network, you will lose your asset.",
        "modalCofirm":"Confirm the transfer to this wallet.",
        "modalInput":"WALLET BEP20",
        "modalcheck": "Verify wallet"
    },
    "buttons": {
        "clear": "Clear",
        "next": "Next",
        "downloadContract": "Download Contract",
        "woWallet": "i don't own a wallet",
        "wWallet": "i already have a wallet",
        "continue": "CONTINUE",
        "tutorials": "WATCH TUTORIAL",
        "seeMore":"See more",
        "modalButtonText": "Send Land",
        "modalButtonText2": "Send",
        "goBack": "Return",
        "wconnect": "Connect with WalletConnect",
        "metamask": "Connect with MetaMask",
        "translate": "Language",
        "btnModalConfirm": "confirm",
        "btnModalCancel": "Cancel",
        "android": `
            <strong>Android</strong>
            <p>
                Tutorial
            </p>
      `,
        "ios": `
        <strong>IOS</strong>
        <p>
            tutorial
        </p>
    `,
    
    },
    "index":{
        "sidebar": `
        We are the <span>
         alliance ${" "}
        </span> that will bring the Metaverse to  <span>the largest buying community in the world.</span>`,
        "heroTitle": "Welcome to the Metaverse!",
        "heroText": `
Here you can claim your lands located on 
        <strong>${" "}LANDIAN </strong>,  Please, have ready your wallet to receive the lands. Don't worry if you don't have a wallet. Go to  <strong>${" "}I don't have a wallet${" "} </strong> and follow the steps to create one`,
        "helpText": `you don't own a wallet ? we've got you cover, you can use our help to create and install a crypto wallet in a few simple steps.`
    },
    "login":{
        "title": "How do I Log In?t",
        "text": `
        We are the <span>
        alliance ${" "}
       </span> that will bring the Metaverse to<br />   <span>the world's largest buying community.</span>`,
        "tutorial": "How do I Log In?"
    },
    "errors":{
        "noUser":"",
        "unverifiedUser":"",
        "noIdentification":"",
        "noRealName":"",
        "noSignature":"",
        "noKYC":"",
    },
    "faqs":{
        "title": "XIFRA FAQ",
        "1": {
            "title":"How long will it take to receive my lands?",
            "text":"You are automatically placed on the waiting list once you log in to the platform. The waiting time is subject to the occupation of the Binance Smart Chain network; They can be sent immediately or take around 15 days. These times are network related and do not depend on Landian or Xifra."
        },
        "2": {
            "title":"Can I sell my land?",
            "text":`
                Of course. You have two options:
                <ol type="1"> 
                    <li>
                        You can sell them through a marketplace like Opensea or Element, where you can reach a global market.
                    </li>
                    <li>
                        You can tell your friends, family, and people close to you about the Landian metaverse and sell them directly; we call this a Per to Per (P2P) transaction. You can send the lands from the dashboard where it says "Send Lands" or directly from your wallet to the buyer's wallet.
                    </li>
                </ol>
            `
        },
        "3": {
            "title":"How much does a Landian land worth?",
            "text": `The value of the land depends on the market. As of today (February 2023), people trade them on the P2P (person to person) market for $200 to $300 per land.
Their value is determined by Landian's adherence to its development roadmap, community adoption, and the global reach they achieve.`
        },
        "4": {
            "title":"Should I put my lands up for sale?",
            "text":`Although this is a personal decision, here are some facts for you to consider:
In April 2022, a 10x10 (100 m2) land in Landian was worth 20 USDT. Less than a year later, the value of the lands has increased at least ten times, thanks to how Landian has positioned itself in a larger community that adopts and supports the project.`
        },
        "5": {
            "title":"Where can i see my lands?",
                "text":`Sometimes, it takes a while for the metadata to load due to network congestion, but don't worry! The fact that you cannot see them does not mean you do not have them in your wallet.
                    If you cannot see the lands in your wallet, follow these steps: Copy your BNB Smart Chain address, search BSC SCAN in your browser, and paste your wallet address there; in the ERC-721 section, you can see all the NFTs you currently store in your wallet. The information shown there is incorruptible, so you can trust that the lands are in your possession.
                    <br/>
                    <br/>
                    Now, if you have a Metamask wallet, the lands will not appear automatically in the NFT section. Follow the next tutorial to add them manually:
                    <br/><br/>
                    <strong>Remember:</strong>
                    <ul>
                        <li>We work on the Binance Smart Chain network; therefore, you should confirm that your wallet is on this network.</li>
                        <li>We recommend using Trust Wallet. However, you can use other wallets, such as Metamask.</li>
                    </ul>
            `
        },
        "6": {
            "title":"How to ask for support?",
            "text":`
                If you have any additional questions, contact the support team through the official Telegram channel: <a href="https://t.me/helplandianxifra" target="_blank" rel="noopener">https://t.me/helplandianxifra </a> 
            `
        },
        "7": {
            "title":"¿Cómo puedo enviar mis lotes en el mercado P2P (persona a persona)?",
            "text":`
                You can do it in several ways, one of them is from your wallet, you enter the NFT section of your wallet, open the NFT you received and choose the "send" option, you must copy the destination wallet of the NFT you are going to send, remember that the destination wallet must also be from the BEP20 network and you must have BNB Smart Chain in your wallet for the network to charge for using it for transactions.
Another way to do it is from your claim dashboard: <a href="https://claim.xifraglobal.com/" target="_blank" rel="noopener">https://claim.xifraglobal.com/</ a>, once you have done the process and you have the NFT in your wallet, select “send land”, paste the destination wallet of the BEP20 network and select send, remember that you must have BNB Smart Chain in your wallet for the payment that the network does for using it for transactions, when you do this in your wallet you must approve the transactions from your wallet and the process would be complete. 
            `
        },
    }
}