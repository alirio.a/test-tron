import { TranslateInterface } from '../../models/translate';
import {User} from "@/models/user";

export const spanishTranslate: TranslateInterface = {
    "signature": {
        "signContract": {
            "contractTitle": "CONTRATO DE DEVOLUCIÓN DE CAPITAL",
            "contractSubtitle": "PAGO CON LOTES DEL METAVERSO DE LANDIAN",
            "signSubtitle":"Por favor, realice su firma digital",
            "uploadSignSutitle":"Cargar firma",
            "realNameInputText": "Nombre completo del acreedor",
            "identificationInputText": "Agrega tu número de documento",
            "placeholderSign": "Firma aquí",
            "initialPart": "Yo",
            "idPart": ", identificado con el número de documento oficial",
            "middlePart":", y registrado en la plataforma con el",
            "lastPart": (user: User)=> (`
            nombre ${user.name} con la suscripción de este Acuerdo, voluntariamente acepto ${user.amount_land} lotes de terreno dentro del metaverso de Landian como pago por los adeudos que tengo en contra de Xifra y que de conformidad con mi wallet hoy en día ascienden a la cantidad de ${user.amount_debt} USD DOLARES AMERICANOS, asimismo declaro paz y salvo y libero a Xifra de cualquier responsabilidad económica presente o futura de cualquier índole.
                <br/><br/>
                Reconozco en este acto que LANDIAN no es, ni será responsable de la obligación existente y adeudada por Xifra a mi persona, si no que su responsabilidad se limita a la funcionalidad de los lotes dentro del Metaverso.
                <br/><br/>
                A través de este acto se define que un (1) lote digital del metaverso de Landian, equivale a más de $200 dólares americanos, para efectos de esta transacción.
                <br/><br/>
                Después de firmar el presente documento y adjuntar la información requerida, la entrega de los lotes se realizará de forma automática.
                <br/><br/>
                Agradeciendo la atención prestada, este documento se suscribe por el acreedor.
            `),
    },
    "kyc": {
        "kycText": `Ahora, necesitamos una foto de tu rostro junto a tu documento de identidad (vista de frente), 
            para ello puedes tomar la foto o cargarla desde tu biblioteca de imágenes. A continuación, 
            te mostramos un ejemplo de como debe ser la foto que se subirá.`,
        "kycSubtitle": "¿QUE DEBES TENER EN CUENTA?",
        "kycTakePhotoSubtitle": "Tomar foto",
        "kycUploadPhotoSubtitle": "Cargar foto",
        "kycPreviewSubtitle": "Imagen de referencia",
        "kycList": `
            <ol type='1'>
            <li>
            Asegúrate que la foto esta enfocada.
            </li>
            <li>
            El documento debe ser totalmente visible.
            </li>
            <li>
            Deben verse tus brazos y tu torso.
            </li>
            <li>
            La cabeza no debe estar cortada.
            </li>
        </ol>`
        },
        "congrats": {
            "congratsTitle": "¡Todo listo!",
            "congratsText": (user) => (`
            Gracias por realizar la firma virtual, el proceso de validación puede tardar entre 1 a 15 días.
            Una vez este confirmado, se hará envío de un correo con las instrucciones 
            para reclamar tus lotes al correo ${user.email}.
            `)
        }
    },
    "stepByStep": {
        "step1":{
            "title": "Paso 1",
            "text1": `Es necesario que cuentes con una billetera, es por esto que te recomendamos <span>${" "} Trust Wallet </span> ${" "}`,
            "text2": `
            <ol type={"A"} style={{ "marginTop": "50px", "fontSize": "20px", "fontWeight": "700", "padding": "22px" }}>
                <li>
                        En la tienda de aplicaciones de tu dispositivo busca  <strong>${" "} Trust Wallet </strong>. 
                </li>
                <li>
                       Presiona el botón Instalar / Obtener.
                </li>
                <li>
                        Abre la aplicación.
                </li>
            </ol>
            `
        },
        "step2":{
            "title": "Paso 2",
            "text1": `Una vez estés en la aplicación debes de crear una cuenta, para ello:`,
            "text2": `
            <ol type={"A"} style={{ "marginTop": "50px", fontSize: "20px", fontWeight: "700", padding: "22px" }}>
                <li>
                        Toca en Crear una nueva wallet.
                </li>
                <li>
                       Acepta los Términos y Condiciones y toca en Continuar.
                </li>
            </ol>
            `
        },
        "step3":{
            "title": "Paso 3",
            "text1": `Copia tu Frase Secreta y guárdala en un lugar seguro. 
No la compartas con nadie. En esta wallet se encuentran todos los activos digitales que en ella albergues. Una vez perdida la Frase Secreta, la cuenta no podrá ser recuperada.`,
            "text2": `¡No la pierdas, no la compartas!`
        },
        "step4":{
            "title": "Paso 4",
            "text1": `Verifica tu frase secreta según el orden dado anteriormente. Recuerda, con tu frase secreta puedes abrir tu billetera en cualquier dispositivo móvil.`,
            "text2": `<span>¡Y listo! </span> Tu cuenta ha sido creada satisfactoriamente y ya puedes reclamar tus lotes. `,
            "text3": `A continuación vamos a iniciar sesión.`
        },
        "step5":{
            "title": "Paso 5",
        }
    },
    "dashboard": {
        "salute": "Hola",
        "account": "Wallet:",
        "seedCapital": "Mi capital semilla",
        "seedCapitalText": "Este valor corresponde a tus lotes reclamados",
        "claimedLands": "Lotes reclamados",
        "unclaimedLands": "Lotes en proceso de reclamación",
        "unclaimedLandsText":`Este proceso puede tardar de 1 a 15 días. <br/> Recibirás una notificación via mail cuando hayas recibido tus Lotes.`,
        "aboutLandian": `<span>Conoce mas sobre el</span>
        <br /> METAVERSO DE LANDIAN`,
        "landianLand":"Landian Land",
        "lastLandsClaimed":"Últimos lotes reclamados",
        "tx": "Transacción",
        "noTx": "NO HAY TRANSACCIONES",
        "lands": "Lotes",
        "landianAssets": "Contrato assets Landian",
        "modalTitle": "Transferir NFTs",
        "modalCardText": "Asegúrate que la dirección de envío sea admitida en la red Binance Smart Chain. Si la dirección de envío no admite esta red, perderás tu activo.",
        "modalCofirm":"Confirma la transferencia a esta wallet.",
        "modalInput":"CARTERA BEP-20",
        "modalcheck": "Verificar wallet"
    },
    "buttons": {
        "clear": "Limpiar",
        "next": "Siguiente",
        "downloadContract": "Descargar Contrato",
        "woWallet": "No tengo una Wallet",
        "wWallet": "Ya tengo una wallet",
        "continue": "CONTINUAR",
        "seeMore":"Ver más",
        "tutorials": "VER TUTORIAL",
        "modalButtonText": "Enviar Land",
        "modalButtonText2": "Enviar",
        "goBack": "VOLVER",
        "wconnect": "Conectar con WalletConnect",
        "metamask": "Conectar con MetaMask",
        "translate": "Idioma",
        "btnModalConfirm": "Confirmar",
        "btnModalCancel": "Cancelar",
        "android": `
            <p>
                Tutorial
            </p>
            <strong>Android</strong>
        `,
        "ios": `
            <p>
                tutorial
            </p>
            <strong>IOS</strong>
        `
    },
    "index":{
        "sidebar": `
        Somos la <span>
          alianza ${" "}
        </span> que trae el metaverso a la comunidad de compradores <span>más grande del mundo. </span>`,
        "heroTitle": "¡Bienvenido al Metaverso!",
        "heroText": `Aquí puedes reclamar tus lotes ubicados en <strong>${" "}LANDIAN </strong>. Ten tu wallet a la mano para recibir tus NFTS. Si no tienes una wallet, no te preocupes. Pulsa <strong>${" "}No tengo una wallet${" "}</strong> y sigue el paso a paso para crear tu nueva billetera crypto.`,
        "helpText": `¿No tienes una wallet?, no te preocupes, te ayudaremos a crear e instalar una cartera crypto en unos simples pasos`
    },
    "login":{
        "title": "Conecta tu wallet",
        "text": `Somos la
        <span>
            ${" "} alianza ${" "}
        </span>
        que llevará el metaverso a la <br /> comunidad de compradores
        <span>
            ${" "}más grande del mundo.
        </span>`,
        "tutorial": "¿Cómo me conecto?"
    },
    "errors":{
        "noUser":"",
        "unverifiedUser":"",
        "noIdentification":"",
        "noRealName":"",
        "noSignature":"",
        "noKYC":"",
    },
    "faqs":{
        "title": "FAQ XIFRA",
        "1": {
            "title":"¿Cuánto tardaré en recibir mis lotes?",
            "text":"En el momento en que ingresaste a la plataforma te agregaste a la cola de espera de entrega de los lotes, el tiempo de espera depende únicamente de la ocupación de la red de Binance Smart Chain; puede ser inmediato, o tardar 15 días, son tiempos de la red y no dependen ni de Landian, ni de Xifra."
        },
        "2": {
            "title":"¿Puedo vender mis lotes?",
            "text":`
                Claro que si, tienes 2 opciones viables para vender tus lotes:
                <ol type="1"> 
                    <li>
                        Ponerlos en un marketplace como Opensea o Element donde estarán abiertos a un mercado global.
                    </li>
                    <li>
                        Contarle a tus amigos, familiares o personas cercanas acerca del metaverso de Landian, y venderles directamente el lote enviandolos desde tu wallet a la del comprador; a esto le llamamos mercado Per to Per (P2P), o persona a persona.
                        Puedes enviar el Lote desde este dashboard donde dice “Enviar Lote”, sino directamente desde tu billetera crypto también lo puedes enviar.
                    </li>
                </ol>
            `
        },
        "3": {
            "title":"¿Cuánto vale un lote de Landian?",
            "text": `El valor de los lotes en Landian depende del mercado, a la fecha (febrero 2023) los lotes de Landian se comercializan en el mercado P2P (persona a persona) por un valor de 200 a 300 dólares por lote.
                La valorización de los Lotes está determinada por el cumplimiento de Landian de su roadmap de desarrollo, la adopción de la comunidad y el alcance global que vaya teniendo el proyecto.`
        },
        "4": {
            "title":"¿Debería poner mis lotes en venta?",
            "text":`Aunque es una decisión personal, te ponemos algunos datos para que lo analices:
                En abril del 2022 un lote de 10x10 (100 m2) tenía un valor de 20 USDT, hoy menos un año después, el valor de los lotes se ha incrementado al menos 10 veces gracias a que Landian ha logrado posicionarse en una comunidad más grande que adoptó y apoya el proyecto.`
        },
        "5": {
            "title":"¿Dónde puedo ver mis lotes?",
            "text":`
                En ocasiones, la carga de la metadata se tarda un poco en llegar por la congestión en la red, ¡no te preocupes! el hecho de que no puedas ver los lotes no significa que no los tengas en tu wallet, si no puedes verlos en la sección de NFT de tu wallet, puedes tomar la dirección de BNB Smart Chain de tu wallet, poner en tu buscador BSCSCAN, pegar allí tu wallet y en la sección de ERC-721 podrás ver todos los NFT que hay en tu wallet, la información que ves allí es incorruptible, por lo tanto puedes confiar que tus lotes estan en tu posesión.
                <br/>
                <br/>
                Ahora bien, si tienes la wallet metamask, los lotes no van a aparecer de manera automática en la sección de NFT, debes agregarlo de manera manual, para esto debes seguir el siguiente tutorial:
                <br/><br/>
                <strong>Cosas a recordar:</strong>
                <ul>
                    <li>Trabajamos en la red de Binance Smart Chain, por lo tanto, debes asegurarte que tu wallet esté en esta red.</li>
                    <li>Recomendamos utilizar Trust Wallet, sin embargo puedes usar otras wallets como Metamask.</li>
                </ul>
            `
        },
        "6": {
            "title":"¿Cómo solicitar ayuda?",
            "text":`
                Recuerda que puedes contactar al equipo de soporte por medio del canal oficial de Telegram si tienes alguna duda adicional: <a href="https://t.me/helplandianxifra" target="_blank" rel="noopener">https://t.me/helplandianxifra </a> 
            `
        },
        "7": {
            "title":"¿Cómo puedo enviar mis lotes en el mercado P2P (persona a persona)?",
            "text":`
                Puedes hacerlo de varias formas, una de ellas es desde tu wallet, entras a la sección de NFT de tu wallet, abres el NFT que recibiste y eliges la opción “enviar”, debes copiar la wallet de destino del NFT que vas a enviar, recuerda que la wallet de destino también debe ser de la red BEP20 y debes tener BNB Smart Chain en tu wallet para el cobro que hace la red por usarla para la transacción.
Otra forma de hacerlo es desde tu dashboard de reclamación: <a href="https://claim.xifraglobal.com/" target="_blank" rel="noopener">https://claim.xifraglobal.com/</a> , una vez hayas hecho el proceso y tengas el NFT en tu wallet, seleccionas “enviar land”, pegas la wallet de destino de la red BEP20 y seleccionas enviar, recuerda que debes tener BNB Smart Chain en tu wallet para el cobro que hace la red por usarla para la transacción, cuando hagas esto en tu wallet debes aprobar la transacción desde tu wallet y el proceso estaría completado. 
            `
        },
    }
}