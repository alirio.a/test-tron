import { FC, ReactNode, createContext, useEffect, useReducer } from 'react';
import type { User } from '@/models/user';
import PropTypes from 'prop-types';
import { createweb3Modal } from '@/utils/web3Modal/createWeb3Modal';
import { providers } from 'ethers';
import Web3Modal from 'web3modal';
import { useRouter } from 'next/router';
import { UserService } from '@/services/user.service';
import { toast } from 'react-toastify';

interface AuthState {
  isInitialized: boolean;
  isAuthenticated: boolean;
  account: string | null;
  walletType: string | null;
  provider: any;
  user: User | null;
}

interface AuthContextValue extends AuthState {
  method: 'Web3';
  loginMetamask: (web3Modal: any, token?: any) => Promise<void>;
  loginWalletConnect: (token?: any) => Promise<void>;
  logout: () => Promise<void>;
  checkToken: (token: string) => Promise<any>;
}

interface AuthProviderProps {
  children: ReactNode;
}

type InitializeAction = {
  type: 'INITIALIZE';
  payload: {
    isAuthenticated: boolean;
    account: string | null;
    walletType: string | null;
    provider: any;
    user: User | null;
  };
};

type LoginAction = {
  type: 'LOGIN';
  payload: {
    isAuthenticated: boolean;
    account: string;
    walletType: string;
    provider: any;
    user: User;
  };
};

type LogoutAction = {
  type: 'LOGOUT';
};

type UnregisteredUserAction = {
  type: 'UNREGISTERED';
  payload: {
    user: User;
  };
};

type Action =
  | InitializeAction
  | LoginAction
  | LogoutAction
  | UnregisteredUserAction;

const initialAuthState: AuthState = {
  isAuthenticated: false,
  isInitialized: false,
  account: null,
  walletType: null,
  provider: null,
  user: null
};

const handlers: Record<
  string,
  (state: AuthState, action: Action) => AuthState
> = {
  INITIALIZE: (state: AuthState, action: InitializeAction): AuthState => {
    const { isAuthenticated, user, account, walletType, provider } =
      action.payload;

    return {
      ...state,
      isAuthenticated,
      isInitialized: true,
      user,
      account,
      walletType,
      provider
    };
  },
  LOGIN: (state: AuthState, action: LoginAction): AuthState => {
    const { user, account, walletType, provider } = action.payload;

    return {
      ...state,
      isAuthenticated: true,
      user,
      account,
      walletType,
      provider
    };
  },
  LOGOUT: (state: AuthState): AuthState => ({
    ...state,
    isAuthenticated: false,
    provider: null,
    walletType: null,
    account: null,
    user: null
  }),
  UNREGISTERED: (state: AuthState, action: UnregisteredUserAction) => ({
    ...state,
    user: action.payload.user
  })
};

const reducer = (state: AuthState, action: Action): AuthState =>
  handlers[action.type] ? handlers[action.type](state, action) : state;

export const Web3Context = createContext<AuthContextValue>({
  ...initialAuthState,
  method: 'Web3',
  loginMetamask: () => Promise.resolve(),
  loginWalletConnect: () => Promise.resolve(),
  logout: () => Promise.resolve(),
  checkToken: () => Promise.resolve()
});

export const AuthProvider: FC<AuthProviderProps> = (props) => {
  const { children } = props;
  const [state, dispatch] = useReducer(reducer, initialAuthState);
  const router = useRouter();

  const handleEvents = async (provider: any) => {
    await provider.on("disconnect", () => {
      console.log("disconnect");
      logout();
    });
    await provider.on("accountsChanged", () => {
        console.log("accountsChanged");
    });
    await provider.on("chainChanged", () => {
        console.log("chainChanged");
    });
  }

  useEffect(() => {
    const initialize = async (): Promise<void> => {
      try {
        const web3Modal = new Web3Modal(createweb3Modal);
        if (
          web3Modal &&
          web3Modal.cachedProvider &&
          window.ethereum &&
          window.ethereum.isMetaMask &&
          window.ethereum.selectedAddress
        ) {
          const provider = await web3Modal.connectTo('injected');
          await provider.on("disconnect", () => {
            console.log("disconnect");
            logout();
          });
          await provider.on("accountsChanged", () => {
            console.log("accountsChanged");
          });
          await provider.on("chainChanged", () => {
            console.log("chainChanged");
          });
          const ethersProvider = new providers.Web3Provider(provider);
          const userAddress = await ethersProvider.getSigner().getAddress();
          let user = null;
          // const data = await UserService.validateUser(
          //   userAddress.toUpperCase()
          // );
/*          let user = null;
          if (data.status) {
            user = data.data;
          } else {

            console.error('Lo sentimos no hemos podido conseguir este usuario');
            console.log("ERROR", 'Lo sentimos no hemos podido conseguir este usuario');
            toast.error('Lo sentimos no hemos podido conseguir este usuario');
          }*/
          dispatch({
            type: 'INITIALIZE',
            payload: {
              isAuthenticated: true,
              provider,
              account: userAddress,
              walletType: 'METAMASK',
              user
            }
          });
        } else if (web3Modal && web3Modal.cachedProvider) {
          const provider = await web3Modal.connect();
          await provider.on("disconnect", () => {
            console.log("disconnect");
            logout();
          });
          await provider.on("accountsChanged", () => {
            console.log("accountsChanged");
          });
          await provider.on("chainChanged", () => {
            console.log("chainChanged");
          });
          const ethersProvider = new providers.Web3Provider(provider);
          const userAddress = await ethersProvider.getSigner().getAddress();
          let user = null;
          /*const data = await UserService.validateUser(
            userAddress.toUpperCase()
          );
          let user = null;
          if (data.status) {
            user = data.data;
          } else {

            console.error('Lo sentimos no hemos podido conseguir este usuario');
            toast.error('Lo sentimos no hemos podido conseguir este usuario');
          }*/

          dispatch({
            type: 'INITIALIZE',
            payload: {
              isAuthenticated: true,
              provider,
              account: userAddress,
              walletType: 'TRUST',
              user
            }
          });
        } else {
          dispatch({
            type: 'INITIALIZE',
            payload: {
              isAuthenticated: false,
              provider: null,
              account: null,
              walletType: null,
              user: null
            }
          });
        }
      } catch (err) {
        console.error(err);
        dispatch({
          type: 'INITIALIZE',
          payload: {
            isAuthenticated: false,
            provider: null,
            account: null,
            walletType: null,
            user: null
          }
        });
      }
    };

    initialize();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const loginWalletConnect = async (
  ): Promise<void> => {
    const web3Modal = new Web3Modal(createweb3Modal);
    const provider = await web3Modal.connect();
    await handleEvents(provider);
    const ethersProvider = new providers.Web3Provider(provider);
    const userAddress = await ethersProvider.getSigner().getAddress();
    let user = null;
    /*if (token) {
      const validationTokenData = await UserService.validateUserByToken(token);
      if(validationTokenData.status){
        if(!validationTokenData.data.verification){
          logout();
          toast.error('Lo sentimos, este usuario no se encuentra verificado');
          return;
        }
      }
      const data = await UserService.setWalletByClient(userAddress, token);
      if (data.status) {
        user = data.data;
      } else {
        logout();
        console.error('Lo sentimos no hemos podido conseguir este usuario');
        toast.error('Lo sentimos no hemos podido conseguir este usuario');
      }
    } else {
      const data = await UserService.validateUser(userAddress);
      if (data.status) {
        user = data.data;
      } else {
        logout();
        console.error('Lo sentimos no hemos podido conseguir este usuario');
        toast.error('Lo sentimos no hemos podido conseguir este usuario');
      }
    }*/
    dispatch({
      type: 'LOGIN',
      payload: {
        isAuthenticated: true,
        provider,
        account: userAddress,
        walletType: 'TRUST',
        user
      }
    });
  };

  const logout = async (): Promise<void> => {
    const web3Modal = new Web3Modal(createweb3Modal);
    web3Modal.clearCachedProvider();

    dispatch({ type: 'LOGOUT' });
    router.push('/');
  };

  const loginMetamask = async (web3Modal, token?: string): Promise<void> => {
    const provider = await web3Modal.connectTo('injected');
    await handleEvents(provider);
    const ethersProvider = new providers.Web3Provider(provider);
    const signer = ethersProvider.getSigner();
    const userAddress = await signer.getAddress();
    let user = null;
    if (token) {
      const validationTokenData = await UserService.validateUserByToken(token);
      if(validationTokenData.status){
        if(!validationTokenData.data.verification){
          logout();
          toast.error('Lo sentimos, este usuario no se encuentra verificado');
          return;
        }
      }
      const data = await UserService.setWalletByClient(userAddress, token);
      if (data.status) {
        user = data.data;
        dispatch({
          type: 'LOGIN',
          payload: {
            isAuthenticated: true,
            provider,
            account: userAddress,
            walletType: 'METAMASK',
            user
          }
        });
      } else {
        logout();
        console.error('Lo sentimos no hemos podido conseguir este usuario');
        toast.error('Lo sentimos no hemos podido conseguir este usuario');
      }
    } else {
      const data = await UserService.validateUser(userAddress);
      if (data.status) {
        user = data.data;
        dispatch({
          type: 'LOGIN',
          payload: {
            isAuthenticated: true,
            provider,
            account: userAddress,
            walletType: 'METAMASK',
            user
          }
        });
      } else {
        logout();
        console.error('Lo sentimos no hemos podido conseguir este usuario');
        toast.error('Lo sentimos no hemos podido conseguir este usuario');
      }
    }
  };

  const checkToken = async (token) => {
    const { data: user, status } = await UserService.validateUserByToken(token);

    dispatch({
      type: 'UNREGISTERED',
      payload: {
        user
      }
    });
    return status;
  };

  return (
    <Web3Context.Provider
      value={{
        ...state,
        method: 'Web3',
        loginWalletConnect,
        loginMetamask,
        logout,
        checkToken
      }}
    >
      {children}
    </Web3Context.Provider>
  );
};

AuthProvider.propTypes = {
  children: PropTypes.node.isRequired
};

export const AuthConsumer = Web3Context.Consumer;
